# Pulse Sensor Library

Needs an object of class pulse to call the functions.
```cpp
pulse p1;
```
## Functions

### getPulseData()
* Parameters - void
* Return type - int

When this function encounters peak in pulse sensor, it measures time between two peaks. Converts this time to Beats Per Minute.
This function gives BPM at that moment.

#### Example

```cpp
Serial.println(p1.getPulseData());
```


### getAvgPulseData()

* Parameters - void
* Return type - int

When this function encounters peak in pulse sensor, it measures time between two peaks. Converts this time to Beats Per Minute.
This function gives average BPM of approx 10 secs.

#### Example

```cpp
Serial.println(p1.getPulseData());
```


### pulseSensorPinAssignment()

* Parameters - int
* Return type - void

Use this function to change the attached pin of pulse sensor.

#### Example

```cpp
p1.pulseSensorPinAssignment(A1);
```


### pulse()

* Parameters - void
* Return type - void

This is a constructor to initialise the threshold, reset the BPM, average BPM, counter.